package com.data.benjiebot;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class DALHelper {
    /**
     * 
     * @return returns a Properties object list that contains all the properties
     * @throws IOException 
     */
    public static Properties getProperties() throws IOException {
        Properties props = new Properties();
        FileInputStream in = null;

        try {
            String propertiesPath = System.getProperty("user.dir") + "\\db.properties";
            in = new FileInputStream(propertiesPath);
            props.load(in);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (in != null) {
                in.close();
            }
        }

        return props;
    }
}
