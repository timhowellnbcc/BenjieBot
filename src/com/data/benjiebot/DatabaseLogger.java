package com.data.benjiebot;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Properties;
import org.sqlite.JDBC;

/**
 *
 * @author th90
 */
public class DatabaseLogger {

    private String dbpath;
    private String className;

    public DatabaseLogger() {
        try {
            Properties props = DALHelper.getProperties();

            dbpath = props.getProperty("sqlite.dbpath");
            className = JDBC.class.getName();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    /**
     * 
     * @param commandused the name of the command used
     * @param user the name of the user that sent the command
     * @param response the response from the bot
     */
    public void log(String commandused, String user, String response) {
        try {
            if (response.contains("'")) {
                response = response.replace("'", "''");
            }
            String sql = "INSERT INTO logs (commandused, user, response, timestamp) "
                            + "VALUES ('" + commandused + "', '" + user + "', '" + response + "', DATETIME('now', 'localtime'))";
            Class.forName(className);
            try (Connection conn = DriverManager.getConnection(JDBC.PREFIX + dbpath)) {
                try (Statement stmt = conn.createStatement()) {
                    stmt.executeUpdate(sql);
                    conn.close();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    conn.close();
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
