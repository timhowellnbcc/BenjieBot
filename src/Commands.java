import com.data.benjiebot.DatabaseLogger;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Commands extends ListenerAdapter {

    private DatabaseLogger dblog = new DatabaseLogger();

    //#region Methods
    /**
     * 
     * @param e the current message event
     * @param message the message you want to send
     */
    public void sendMessage(MessageEvent e, String message){
        e.getChannel().send().message(message);
    }
    /**
     * 
     * @param e the current message event
     * @param message the message you want to get and convert to lower case
     */
    public String getMessageToLowerCase(MessageEvent e){
        return e.getMessage().toLowerCase();
    }
    /**
     * This method gets the nickname of the current user
     * @param e the current message event
     */
    public String getNick(MessageEvent e){
        return e.getUser().getNick();
    }
    /**
     * 
     * @param e the current message event
     * @param command the command you want to send
     */
    public boolean getCommand(MessageEvent e, String command){
        return e.getMessage().equalsIgnoreCase(command);
    }
    //#endregion
    /**
     * 
     * @param event gets a message event if there is one caught in our bot via the API
     * @override Overrides the onMessage even in ListenerAdapter
     */
    @Override
    public void onMessage(MessageEvent event) {
        //gets a list of mods when a message is recieved
        Pattern urlPattern = Pattern.compile("(https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9]+\\.[^\\s]{2,}|www\\.[a-zA-Z0-9]+\\.[^\\s]{2,})", Pattern.CASE_INSENSITIVE);
        //create a matcher for the regex
        Matcher m = urlPattern.matcher(event.getMessage());

        //Detect if the regex for links finds a link as a submitted message
        //and print a response to the user before timing them out
        //if the user is not a moderator
        //Currently gets the mod status but the regex isn't working
        if (event.getTags().get("mod").equals("0") && m.find()) {
            String response = "Don't send links! (Warning).";
            sendMessage(event, response);
            sendMessage(event , ".timeout " + getNick(event));
        } else if (getCommand(event, "!arena")) {
            String response = "Arena Code is: 6DRKL";
            sendMessage(event, response);
            dblog.log(getMessageToLowerCase(event), getNick(event), response);
        } else if (getCommand(event, "!discord")) {
            String response = "	Feel free to join this channel's Discord: https://discord.gg/QMzZqH6 Kappa";
            sendMessage(event, response);
            dblog.log(getMessageToLowerCase(event), getNick(event), response);
        } else if (getCommand(event, "!fc")) {
            String response = "Benjie's Switch Friend Code is: SW-1021-4929-1227";
            sendMessage(event, response);
            dblog.log(getMessageToLowerCase(event), getNick(event), response);
        } else if (getCommand(event, "!nuzlocke")) {
            String response = "Rules I'm Playing By: "
                            + "1. The first pokemon you encounter in a route you must catch if possible. "
                            + "1.1. I'm going to try to get the random encounters that are present first and foremost. "
                            + "1.2. If I run into an overworld pokemon while attempting 1.1 then that pokemon takes its spot. "
                            + "2. You must name every pokemon 3. If a Pokemon Faints you cannot use it anymore.";
            sendMessage(event, response);
            dblog.log(getMessageToLowerCase(event), getNick(event), response);
        } else if (getCommand(event, "!schedule")) {
            String response = "Mon/Tues 7-10pm and Sat 10am-2pm EST";
            sendMessage(event, response);
            dblog.log(getMessageToLowerCase(event), getNick(event), response);
        } else if (getCommand(event, "!social")) {
            String response = "https://twitter.com/BenjieJay";
            sendMessage(event, response);
            dblog.log(getMessageToLowerCase(event), getNick(event), response);
        } else if (getCommand(event, "!switch")) {
            String response = "Benjie's Switch Friend Code is: SW-1021-4929-1227";
            sendMessage(event, response);
            dblog.log(getMessageToLowerCase(event), getNick(event), response);
        } else if (getCommand(event, "!commands") || getCommand(event, "!help")){
            String response = "Commands: !arena !discord !fc !nuzlocke !schedule !social !switch !commands";
            sendMessage(event, response);
            dblog.log(getMessageToLowerCase(event), getNick(event), response);
        }
    }
}
