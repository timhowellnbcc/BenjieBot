import com.data.benjiebot.DALHelper;

import java.io.IOException;
import java.util.Properties;

import org.pircbotx.Configuration;
import org.pircbotx.PircBotX;
import org.pircbotx.cap.EnableCapHandler;
import org.pircbotx.exception.IrcException;

public class BenjieBot {
    //create a database token access object so that we can get out token
    private static Properties props;

    public static void main(String[] args) throws IOException {
        //make a new instance of BenjieBot
        props = DALHelper.getProperties();
        new BenjieBot();
    }
    /*
        Store in logs table:
            [id] - ID of command
            [commandused] - Command sent by the user,
            [user] - Name of the user that sent the command,
            [response] - the response sent by the bot
            [timestamp] - Date and time of the command sent
     */
    //create a new instance of pircbotx Configuration and build a config for the bot
    private Configuration benjiebotconfig = new Configuration.Builder()
                    .setAutoNickChange(false) //Twitch doesn't support multiple users
                    .setOnJoinWhoEnabled(false) //Twitch doesn't support WHO command
                    .setCapEnabled(true)
                    .addCapHandler(new EnableCapHandler("twitch.tv/membership")) //Twitch by default doesn't send JOIN, PART, and NAMES unless you request it, see https://dev.twitch.tv/docs/irc/guide/#twitch-irc-capabilities
                    .addCapHandler(new EnableCapHandler("twitch.tv/tags"))
                    .addCapHandler(new EnableCapHandler("twitch.tv/commands"))
                    .addServer("irc.twitch.tv", 6667)
                    .setName("benjiebot")
                    .setServerPassword(props.getProperty("twitch.token"))
                    .addAutoJoinChannel("#benjiejay")
                    .addListener(new Commands())
                    .buildConfiguration();
    //default constructor
    public BenjieBot() {
        //create a new instance of pircbotx on creation of BenjieBot with our configuration 
        PircBotX bot = new PircBotX(benjiebotconfig);
        try {
            bot.startBot();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IrcException e) {
            e.printStackTrace();
        }
    }
    
}
